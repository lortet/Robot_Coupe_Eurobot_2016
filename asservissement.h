//----------------------------------------------------------------------//
//                            ASSERVISSEMENT                            //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h17  //
//----------------------------------------------------------------------//
//  Permet de déplacer le robot à l'aide d'une commande en rotation et  //
//  distance.                                                           //
//----------------------------------------------------------------------//
//                                 Benjamin BROSSEAU & Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//        Adapté des travaux de Kevin LE MOGUEN et Thibault d'ARTIGUES  //
//----------------------------------------------------------------------//

#ifndef __ASSERVISSEMENT_H__                            // Si la constante n'est pas définie
#define __ASSERVISSEMENT_H__                            // On la définie de manière à ce que le fichier ne soit plus inclus
    
    
    #define SATURATION_COMMANDE 70                          // Défini le niveau de saturation de la commande
    #define SEUIL_COMMANDE 10                               // Defini le seuil de commande pour éviter le tremblements
    
    #define STATE_ARRET 0                                   // Mouvement terminé
    #define STATE_TRANSLATION 1                             // Translation en cours et rotation terminée
    #define STATE_ROTATION 2                                // Rotation en cours
    
    #define KpTra 0.02
    #define KiTra 0
    #define KdTra 0
    
    #define KpRot 0.15
    #define KiRot 0
    #define KdRot 0
    
    #define NB_TICKS_PAR_M 215529                           // Défini le nombre de tick pour avancer d'un mètre(valeur doublé : 207400)
    #define NB_TICKS_PAR_TOUR 89226                         // Défini le nombre de tick pour tourner d'un tour
    
    #include "Arduino.h";                                   //Utilisation de la fonction abs()
    #include "moteurs.h";                                   //Utilisation de setMoteurDroit() et setMoteurGauche()
    #include "codeur.h";                                    //Utilisation de getPosition()
    
    class Asservissement{
      private:
        volatile char state;                                // Stocke l'étape du mouvement
        volatile long consigneTra;                          // Stocke la consigne en translation
        volatile long consigneRot;                          // Stocke la consigne en rotation
    
      public:
        Asservissement();                                   // Initialisation
        void setConsigneTra(long translation);              // Permet de définir la consigne en translation
        void setConsigneRot(long rotation);                 // Permet de définir la consigne en rotation
        void setConsigne(long rotation, long translation);  // Permet de définir la consigne en rotation et translation
        void addConsigneTra(long translation);              // Permet d'ajouter des contraintes à la consigne actuelle en translation
        void addConsigneRot(long rotation);                 // Permet d'ajouter des contraintes à la consigne actuelle en rotation
        void addConsigne(long rotation, long translation);  // Permet d'ajouter des contraintes à la consigne actuelle en rotation et translation
        void step();                                        // Permet d'effectuer un pas de l'asservissement
        char getState();                                    // 0 : A terminé le mouvement actuel; 1 : En cours de translation; 2 : En cours de rotation; X : Indéfini
        void allowCircle();                                 // Autorise la rotation et la translation en simultané
        void forceStop();                                   // Permet de forcer la fin du mouvement actuel
    };
    
    
#endif