//----------------------------------------------------------------------//
//                               MOVEMENT                               //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h15  //
//----------------------------------------------------------------------//
//  Permet de déplacer le robot suivant un système d'axes               //
//----------------------------------------------------------------------//
//                                                     Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//----------------------------------------------------------------------//

#ifndef __MOOVE_H__                                      // Si la constante n'est pas définie
#define __MOOVE_H__                                      // On la définie de manière à ce que le fichier ne soit plus inclus
    
    
    #include "Arduino.h";                                                  //Utilisation de la constante PI
    #include "asservissement.h";                                           //Utilisation de Asservissement
    #include "odometrie.h";                                                //Utilisation de Odometrie()
    #include "position.h"                                                  //Utilisation de Position
    
    class Movement{
    
      private:
        Odometrie odom;
        Asservissement asser;
        void calculParams(int x, int y, int *rotation, int *translation);  //Retourne les valeurs de rotation et de translation par rapport à des coordonnées
      
      public:
        void goToXY(int x, int y);                                         //Se rend aux coordonnées demandées
        void step();                                        // Permet d'effectuer un pas de l'asservissement
        char getState();                                    // 0 : A terminé le mouvement actuel; 1 : En cours de translation; 2 : En cours de rotation; X : Indéfini
        void allowCircle();                                 // Autorise la rotation et la translation en simultané
        void forceStop();                                   // Permet de forcer la fin du mouvement actuel
        Position getPos();
    };
    
    
#endif