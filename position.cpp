//----------------------------------------------------------------------//
//                               POSITION                               //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h06  //
//----------------------------------------------------------------------//
//  Permet de définir un objet de type position                         //
//----------------------------------------------------------------------//
//                                                     Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//----------------------------------------------------------------------//

#include "position.h"          //Utilisation de Position  

Position::Position(){
  this->x = 0;
  this->y = 0;
  this->O = 0;
}
double Position::getX() {
  return x;
}
double Position::getY(){
  return y;
}
double Position::getO(){
  return O;
}
void Position::addX(double x) {
  this->x += x;
}
void Position::addY(double y){
  this->y += y;
}
void Position::addO(double O){
  this->O += O;
}
void Position::setX(double x) {
  this->x = x;
}
void Position::setY(double y){
  this->y = y;
}
void Position::setO(double O){
  this->O = O;
}