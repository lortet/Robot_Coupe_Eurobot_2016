//----------------------------------------------------------------------//
//                            ASSERVISSEMENT                            //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h17  //
//----------------------------------------------------------------------//
//  Permet de déplacer le robot à l'aide d'une commande en rotation et  //
//  distance.                                                           //
//----------------------------------------------------------------------//
//                                 Benjamin BROSSEAU & Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//        Adapté des travaux de Kevin LE MOGUEN et Thibault d'ARTIGUES  //
//----------------------------------------------------------------------//

#include "asservissement.h";  //Utilisation de Asservissement()

Asservissement::Asservissement(){
  this->state = STATE_ARRET;
  this->setConsigne(0, 0);
}

void Asservissement::setConsigneTra(long translation){
  this->consigneTra = translation;
  this->state = STATE_ROTATION;
}

void Asservissement::setConsigneRot(long rotation){
  this->consigneRot = rotation;
  this->state = STATE_ROTATION;
}

void Asservissement::setConsigne(long rotation, long translation){
  this->setConsigneRot(rotation);
  this->setConsigneTra(translation);
  this->state = STATE_ROTATION;
}

void Asservissement::addConsigneTra(long translation){
  this->consigneTra += translation;
  this->state = STATE_ROTATION;
}

void Asservissement::addConsigneRot(long rotation){
  this->consigneRot += rotation;
  this->state = STATE_ROTATION;
}

void Asservissement::addConsigne(long rotation, long translation){
  this->addConsigneRot(rotation);
  this->addConsigneTra(translation);
  this->state = STATE_ROTATION;
}

void Asservissement::step(){
  long codeurD;
  long codeurG;
  long tempConsigneRot = this->consigneRot;
  long tempConsigneTra = this->consigneTra;

  getPosition(&codeurG, &codeurD);
  

  
  if( this->state == STATE_ROTATION ) tempConsigneTra = (codeurG + codeurD);                                      // Si l'étape de rotation est en cours, on surcharge la commande de translation
  
  //Écart en translation
  float erreurTra = tempConsigneTra - (codeurG + codeurD);
  float SerreursTra = SerreursTra + erreurTra;
  float erreur_precedenteTra = erreurTra;
  float variation_erreurTra = erreurTra - erreur_precedenteTra;

  //commande en translation
  float commandeTra = KpTra * erreurTra + KiTra * SerreursTra + KdTra * variation_erreurTra;
  erreur_precedenteTra = erreurTra;

  //saturation en commande translation
  if (commandeTra > SATURATION_COMMANDE)commandeTra = SATURATION_COMMANDE;

  //ecart en angle
  float erreurRot = tempConsigneRot - (codeurG - codeurD);
  float SerreursRot = SerreursRot + erreurRot;
  float erreur_precedenteRot = erreurRot;
  float variation_erreurRot = erreurRot - erreur_precedenteRot;

  //commande en rotation
  float commandeRot = KpRot * erreurRot + KiRot * SerreursRot + KdRot * variation_erreurRot;
  erreur_precedenteRot = erreurRot;

  //saturation en de la commande en  rotation
  if (commandeRot > SATURATION_COMMANDE)commandeRot = SATURATION_COMMANDE;

  //commande moteur.
  int commandeD = commandeTra - commandeRot;
  int commandeG = commandeTra + commandeRot;
  //la gestion des saturations de commandes est dans le driver du moteur.

  //arret a destination (evite les vibrations).
  if (abs(commandeD) < SEUIL_COMMANDE) commandeD = 0;
  if (abs(commandeG) < SEUIL_COMMANDE) commandeG = 0;
  
  if(this->state == STATE_ROTATION && tempConsigneRot == (codeurG - codeurD) ) this->state = STATE_TRANSLATION;  // Si l'etape de rotation est terminée, on attaque la translation
  else if(this->state == STATE_TRANSLATION && commandeD == 0 && commandeG == 0) this->state = STATE_ARRET;       // Si l'étape de translation est terminée, on arrête le mouvement
  
  setMoteurDroit(commandeD);
  setMoteurGauche(commandeG);
}

char Asservissement::getState(){
  return this->state;
}

void Asservissement::allowCircle(){
  this->state = STATE_TRANSLATION;
}

void Asservissement::forceStop(){
  long codeurD;
  long codeurG;
  
  getPosition(&codeurG, &codeurD);
  this->setConsigne(codeurG + codeurD, codeurG - codeurD);
}