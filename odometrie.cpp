//----------------------------------------------------------------------//
//                               ODOMETRIE                              //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h12  //
//----------------------------------------------------------------------//
//  Permet de repérer le robotsur le plateau à l'aide des codeurs       //
//----------------------------------------------------------------------//
//                                 Benjamin BROSSEAU & Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//        Adapté des travaux de Kevin LE MOGUEN et Thibault d'ARTIGUES  //
//----------------------------------------------------------------------//

#include "odometrie.h"                                     //Utilisation de Odometrie()

Odometrie::Odometrie(){
  this->NbTic_Droit = 0;
  this->NbTic_Gauche = 0;
  this->Nb_Tic_RG_Av = 0;
  this->Nb_Tic_RD_Av = 0;
}
Position Odometrie::getPos(){
  return pos;
}
signed long Odometrie::getNbTic_Droit() {
  return NbTic_Droit;
}
signed long Odometrie::getNbTic_Gauche(){
  return NbTic_Gauche;
}
/* mise a jour la nouvelle position du robot (x, y, O) par approximation de segment de droite */
void Odometrie::position_segment(double distance, double angle)
{
    pos.addO( -((angle/(INIT_ENTRAXE*2))*(2*PI))/VALEUR_PAR_TOUR ); // Calcul de la valeur de l'angle en Radian
   
	if ( pos.getO() > ANGLE_ODO_MAX){
		pos.addO( -2*PI );
	}
	else if ( pos.getO() < ANGLE_ODO_MIN){
		pos.addO( 2*PI );
	}

    Vari_X_F += (double)sin(pos.getO())*distance/TICK_PAR_MM; // Calcul de la variation de X
    Vari_Y_F += (double)cos(pos.getO())*distance/TICK_PAR_MM; // Calcul de la variation de Y
	
    Vari_X = (signed long)Vari_X_F;
  	Vari_Y = (signed long)Vari_Y_F;
	
    Vari_X_F -= Vari_X;
	Vari_Y_F -= Vari_Y;
	
	pos.addX( Vari_X ); 	// Valeur de X  
	pos.addY( Vari_Y ); 	// Valeur de Y
}

 
/* delta_roue_droite et delta_roue_gauche sont les distance en pulses
 * parcourue par les roues droite et gauche en un cycle */
void Odometrie::calcPosition(){
  
  long delta_rg;
  long delta_rd;
  
  getPosition(&NbTic_Gauche,&NbTic_Droit);

  
  delta_rd = NbTic_Droit - Nb_Tic_RD_Av;
  delta_rg = NbTic_Gauche - Nb_Tic_RG_Av;
  
  Nb_Tic_RD_Av = NbTic_Droit; 
  Nb_Tic_RG_Av = NbTic_Gauche; 
  
  double delta_distance = 0, delta_angle = 0;

  delta_distance = ((double)(delta_rd + delta_rg))/2;   // Calcul de la variation de la distance
  delta_angle = (double)(delta_rd - delta_rg)*2;        // Calcul de la variation de l'angle
 
  position_segment(delta_distance, delta_angle);
}

void Odometrie::Reset_Tics(){
  this->NbTic_Droit = 0;
  this->NbTic_Gauche = 0;
  this->Nb_Tic_RG_Av = 0;
  this->Nb_Tic_RD_Av = 0;
}