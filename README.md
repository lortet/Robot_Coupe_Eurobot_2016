## Robot Coupe Eurobot 2016

* Code du robot pour la coupe Eurobot 2016 de l'Université de Nantes
* Benjamin BROSSEAU et Valentin LORTET
* Adapté des travaux de Kevin LE MOGUEN et Thibault d'ARTIGUES
* CC-BY

**Fichiers**

1. Robot_Serinus_2016.ino : Fichier principale Arduino pour le robot de la Coupe Eurobot 2016.

2. movement.cpp : Classe C++ contenant le code spécifique aux deplacements pour la coupe Eurobot 2016.
3. movement.h : Fichier d'entête C++ contenant le code spécifique  aux deplacements pour la coupe Eurobot 2016.

4. asservissement.cpp : Classe C++ contenant le code spécifique à l'asservissement pour la coupe Eurobot 2016.
5. asservissement.h : Fichier d'entête C++ contenant le code spécifique à l'asservissement pour la coupe Eurobot 2016.

6. odometrie.cpp : Classe C++ contenant le code spécifique à l’odométrie pour la coupe Eurobot 2016.
7. odometrie.h : Fichier d'entête C++ contenant le code spécifique à l'odométrie pour la coupe Eurobot 2016.

8. position.cpp : Classe C++ contenant le code spécifique aux informations de position pour la coupe Eurobot 2016.
9. position.h : Fichier d'entête C++ contenant le code spécifique aux informations de position pour la coupe Eurobot 2016.

10. codeur.cpp : Fichier C++ contenant le code spécifique aux codeurs pour les coupes Eurobot 2015 et 2016.
11. codeur.h : Fichier d'entête C++ contenant le code spécifique aux codeurs pour les coupes Eurobot 2015 et 2016.

12. moteurs.cpp : Fichier C++ contenant le code spécifique aux moteurs pour les coupes Eurobot 2015 et 2016.
13. moteurs.h : Fichier d'entête C++ contenant le code spécifique aux moteurs pour les coupes Eurobot 2015 et 2016.