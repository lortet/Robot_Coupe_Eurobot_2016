//----------------------------------------------------------------------//
//                               ODOMETRIE                              //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h12  //
//----------------------------------------------------------------------//
//  Permet de repérer le robotsur le plateau à l'aide des codeurs       //
//----------------------------------------------------------------------//
//                                 Benjamin BROSSEAU & Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//        Adapté des travaux de Kevin LE MOGUEN et Thibault d'ARTIGUES  //
//----------------------------------------------------------------------//

#ifndef __ODOMETRIE_H__
#define __ODOMETRIE_H__

    #define ANGLE_ODO_MAX 3.1415926535
    #define ANGLE_ODO_MIN -3.1415926535
    #define INIT_ENTRAXE 2560
    #define TICK_PAR_MM 103.7
    #define VALEUR_PAR_TOUR 33.224
    
    #include "Arduino.h"                                       //Utilisation de la constante PI
    #include "codeur.h"                                        //Utilisation de getPosition()
    #include "position.h"                                      //Utilisation de Position
    
    class Odometrie{
    
      private:
        Position pos;
        signed long NbTic_Droit;
        signed long NbTic_Gauche;
        long Nb_Tic_RD_Av;
        long Nb_Tic_RG_Av;
        signed long Vari_X_F;
        signed long Vari_Y_F;
        signed long Vari_X;
        signed long Vari_Y;
      
      public:
        Position getPos();
        Odometrie();
        void position_segment(double distance, double angle);  // Cette fonction qui est appelé par la fonction odometrie sert à calculer les paramètres necessaire au calcul des coordonnées
        void calcPosition();                                   // Cette fonction a appeler sert à calculer la position du robot sur le terrain
        void Reset_Tics();                                     // La fonction permettant le reset du nombre de tics des codeurs.
        signed long getNbTic_Droit();
        signed long getNbTic_Gauche();
    };

#endif