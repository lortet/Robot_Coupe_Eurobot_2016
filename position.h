//----------------------------------------------------------------------//
//                               POSITION                               //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h06  //
//----------------------------------------------------------------------//
//  Permet de définir un objet de type position                         //
//----------------------------------------------------------------------//
//                                                     Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//----------------------------------------------------------------------//

#ifndef __POSITION_H__
#define __POSITION_H__

class Position{

  private:
    double x;
    double y;
    double O;
  
  public:
    Position();
    double getX();
    double getY();
    double getO();
    void addX(double x);
    void addY(double y);
    void addO(double O);
    void setX(double x);
    void setY(double y);
    void setO(double O);
};

#endif