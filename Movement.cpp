//----------------------------------------------------------------------//
//                               MOVEMENT                               //
//                                                    version 16.02.03  //
//                                             date 03/02/2016 - 17h15  //
//----------------------------------------------------------------------//
//  Permet de déplacer le robot suivant un système d'axes               //
//----------------------------------------------------------------------//
//                                                     Valentin LORTET  //
//                             Pour l'IUT de Nantes et le club Serinus  //
//----------------------------------------------------------------------//

#include "movement.h";

void Movement::goToXY(int x, int y){
  int translation;
  int rotation;
  
  calculParams(x, y, &translation, &rotation);
  asser.setConsigne(translation, rotation);
}

void Movement::calculParams(int x, int y, int *rotation, int *translation){
  int angle;
  Position pos = odom.getPos();

  int d_x = x - pos.getX();
  int d_y = y - pos.getY();

  int distance = sqrt( d_x*d_x + d_y*d_y );

  if( d_x == 0 ){
    if( d_y >= 0 ) angle = PI/2;
    else angle = -PI/2;
  }
  else angle = tan( d_y / d_x );

  if( d_x < 0 ) angle += PI;

  *rotation = angle - pos.getO();
  *translation = distance;
}

void Movement::step(){
  asser.step();
  odom.calcPosition();
}

char Movement::getState(){
  return asser.getState();
}

void Movement::allowCircle(){
  asser.allowCircle();
}

void Movement::forceStop(){
  asser.forceStop();
}

Position Movement::getPos(){
  return odom.getPos();
}