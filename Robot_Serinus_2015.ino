#include "movement.h";

Movement move;

void setup(){
  pinMode(5,OUTPUT);
  initCodeur();
  initMoteurs();
  Serial.begin(115200);
  Serial3.begin(115200);
}

void loop(){
  int rot, tra;
  
  /*
  move.goToXY(1*NB_TICKS_PAR_M,0);
  do{ 
    move.step();
  }
  while(move.getState() != STATE_ARRET);
  
  
  delay(5000);
  
  
  
  move.goToXY(0,0);
  do{ 
    move.step();
  }
  while(move.getState() != STATE_ARRET);
  */
  
  delay(1000);
}

void serialEvent() {
  boolean stringComplete = false;
  char stringLength = 0;
  String inputString = "";
  
  while (Serial.available() && !stringComplete && stringLength < 16) {
    char inChar = (char)Serial.read();
    
    if (inChar == '\n') stringComplete = true;
    else{
      inputString += inChar;
      stringLength++;
    }
  }
  
  if( inputString == "STOP" ) move.forceStop();
  else if( inputString == "PING" ) Serial.println( "PONG" );
  else Serial.println( inputString );
}

void serialEvent3() {
  boolean stringComplete = false;
  char stringLength = 0;
  String inputString = "";
  
  while (Serial3.available() && !stringComplete && stringLength < 16) {
    char inChar = (char)Serial3.read();
    
    if (inChar == '\n') stringComplete = true;
    else{
      inputString += inChar;
      stringLength++;
    }
  }
  
  if( inputString == "STOP" ) move.forceStop();
  else if( inputString == "PING" ) Serial3.println( "PONG" );
  else Serial3.println( inputString );
}